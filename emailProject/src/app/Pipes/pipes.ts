import { Pipe, PipeTransform } from '@angular/core';
import { email } from '../Models/models'

@Pipe({name: 'searchPipe'})
export class searchPipe implements PipeTransform {
  transform(emails: email[], search: string): email[] {
    return emails.filter((email: email) => { 
		return email.subject.includes(search) || email.body.includes(search);
	});
  }
}

@Pipe({name: 'filterPipe'})
export class filterPipe implements PipeTransform {
  transform(emails: email[], from: string, to: string, date: Date): email[] {
	  let emailsFiltered = emails;
	  if(from) {
		emailsFiltered = emailsFiltered.filter((email: email) => { 
			return email.from.includes(from);
		});
	  }
	  
	  if(to) {
		  emailsFiltered = emailsFiltered.filter((email: email) => { 
			return email.to.includes(to);
		});
	  }
	  
	  if(date) {
		 emailsFiltered = emailsFiltered.filter((email: email) => { 
			return email.date > date;
		});
	  }
	  return emailsFiltered;
    
  }
}

@Pipe({name: 'highlightPipe'})
export class highlightPipe implements PipeTransform {
	transform(list: string, searchText: string): string {
		if (!list) { return ""; }
		if (!searchText) { return list; }

		const re = new RegExp(searchText, 'gi');
		const value = list.replace(re, "<span class='highlight'>" + searchText + "</span>" );
		return value;
	}
}