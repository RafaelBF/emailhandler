import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatDialogModule} from '@angular/material/dialog';

import { DialogContentEmail } from './dialogContentEmail.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PipesModule } from '../../Pipes/pipes.module';

@NgModule({
  declarations: [
    DialogContentEmail
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
	MatTableModule,
	MatCardModule,
	MatDialogModule,
	PipesModule
  ],
  providers: [],
})
export class DialogContentEmailModule { }
