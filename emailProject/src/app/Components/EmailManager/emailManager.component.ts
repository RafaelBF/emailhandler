import { Component, Input, ViewChild } from '@angular/core';
import { email } from './../../Models/models';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { DialogContentEmail } from './../DialogContentEmail/dialogContentEmail.component';
import { EmailService } from '../../Services/emailService';

@Component({
  selector: 'emails',
  templateUrl: './emailManager.component.html',
  styles: [
		'.email-wrapper { margin: 10px; }',
		'table.emails { width: 100%; table-layout: fixed;}',
		'td.email { white-space: nowrap; overflow: hidden; }',
		'td span { margin: 0 5px; }',
		'.subject-content { overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #202124; }',
		'.body-content { overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #5f6368; }'
	]
	})
export class EmailManagerComponent {
  emails: email[];
  search: string = "";
  from: string = "";
  to : string = "";
  displayedColumns: string[] = ['emailContent'];
  dataSource: MatTableDataSource<email> = new MatTableDataSource<email>([]);
  
  @ViewChild(MatPaginator) paginator: any = null;
  
  constructor (
		private _emailService: EmailService,
		public dialog: MatDialog
	) {}
  
  ngOnInit() {
	  this.emails = this._emailService.getEmails().map((emailUnity: any) => {
		  return new email(emailUnity);
	  });
	  
  }
  
  ngAfterViewInit() {
		this.dataSource = new MatTableDataSource<email>(this.emails);
		this.dataSource.paginator = this.paginator;
		this.dataSource.filterPredicate = (data: email , filter: string ): boolean => {
			return (
				data.subject.toLowerCase().includes(this.search.toLowerCase())
				|| data.body.toLowerCase().includes(this.search.toLowerCase())
			) &&
			(
				data.from.toLowerCase().includes(this.from.toLowerCase())
			) &&
			(
				data.to.some(unit => {return unit.toLowerCase().includes(this.to.toLowerCase())})
			);
		}
  }
  
  onEmailSelection(email: email) {
		const dialogRef = this.dialog.open(DialogContentEmail,{data: {
			email: email, 
			search: this.search
		}});
  }
  
	applySearch() {
		this.dataSource.filter = this.search + this.to + this.from;
	}
}

export class tableFilter {
	search: string;
	from: string;
	to: string;
}
