import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { EmailManagerComponent } from './emailManager.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import { DialogContentEmailModule } from '../DialogContentEmail/dialogContentEmail.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PipesModule } from '../../Pipes/pipes.module';

@NgModule({
  declarations: [
    EmailManagerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
	MatTableModule,
	MatPaginatorModule,
	MatCardModule,
	FormsModule,
	DialogContentEmailModule,
	PipesModule
  ],
})
export class EmailManagerModule { }

