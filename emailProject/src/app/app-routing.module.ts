import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailManagerComponent } from './Components/EmailManager/emailManager.component';

const routes: Routes = [
	{path: '', component: EmailManagerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
