import { NgModule } from '@angular/core';
import { filterPipe, searchPipe, highlightPipe } from './pipes';


@NgModule({
  declarations: [
	filterPipe,
	searchPipe,
	highlightPipe
  ],
  imports: [
  ],
  exports: [
	filterPipe,
	searchPipe,
	highlightPipe
  ],
  providers: [],
  bootstrap: []
})
export class PipesModule { }
