export class email {
	from: string;
    to: string[];
    cc: string[];
    bcc: string[];
    subject: string;
    body: string;
    date: Date;
    
	constructor(rawData: any) {
		this.from = rawData.from;
		this.to = rawData.to;
		this.cc = rawData.cc;
		this.bcc = rawData.bcc;
		this.subject = rawData.subject;
		this.body = rawData.body;
		this.date = new Date(rawData.date);
	}
}
