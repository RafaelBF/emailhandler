import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmailService } from './Services/emailService';
import { EmailManagerModule } from './Components/EmailManager/emailManager.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
	EmailManagerModule	
  ],
  providers: [
	EmailService	
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
