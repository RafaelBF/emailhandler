import { Component, Inject } from '@angular/core';
import { email } from './../../Models/models';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'dialog-content-email',
  templateUrl: './dialogContentEmail.component.html',
  styles: [
  '.email-info { width: 35rem; };', 
  '.subject { font-size: 16px; color: #202124; }',
  'table.emails { width: 100%; margin-top: 5px };',
  '.display-info { margin-top: 10px; margin-bottom: 15px; padding: 10px;}'
  ]
})
export class DialogContentEmail {  
  constructor (
	public dialogRef: MatDialogRef<DialogContentEmail>,
    @Inject(MAT_DIALOG_DATA) public data: DialogContent
  ) {}

}

export class DialogContent {
	email: email;
	search: string;
}
